module.exports = function(grunt) {
	require('load-grunt-tasks')(grunt);
	grunt.initConfig({
		concat: {
		dist: {
		  src: ['sass/*.scss'],
		  dest: 'css/style.css',
			},
		},
		watch: {
			css: {
			files: ['sass/**'],
			tasks: ['sass:ar', 'sass:en'],
			options: {
                    spawn: false,
                    livereload: true
                },
			},
			html: {
			files: ['*.html'],
			tasks: [],
			options: {
                    spawn: false,
                    livereload: true,
                },
			},
		},
		sass: {
	        options: {
	            sourceMap: true,
	            outputStyle: "compressed",
	        },
	        ar: {
	            files: {
	                'css/style-ar.css': 'sass/style-ar.scss',
	                '../src/assets/css/style-ar.css': 'sass/style-ar.scss',
	            },
	        },
            en: {
	            files: {
	                'css/style-en.css': 'sass/en/style-en.scss',
	                '../src/assets/css/style-en.css': 'sass/en/style-en.scss',
	            },
	        },
    	},
	});
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('default', ['watch']);
};